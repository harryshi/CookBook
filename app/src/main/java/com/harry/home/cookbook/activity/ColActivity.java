package com.harry.home.cookbook.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;

import com.harry.home.cookbook.R;

/**
 * Created by Administrator on 2016/7/16.
 */
public class ColActivity extends Activity{
    private CollapsingToolbarLayout mCollToolbar;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_col);
        mCollToolbar = (CollapsingToolbarLayout) findViewById(R.id.coll_toolbar);
        mCollToolbar.setTitle("hello Coll");
    }
}
