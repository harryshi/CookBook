package com.harry.home.cookbook.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.harry.home.cookbook.R;

/**
 * 碎片
 * Created by Administrator on 2016/7/16.
 */
public class TestFragment extends Fragment{
    private static TestFragment mFragment;
    private TextView tv;
    public static TestFragment newInstance(String param){
        mFragment = new TestFragment();
        Bundle b = new Bundle();
        b.putString("TAG",param);
        mFragment.setArguments(b);
        return mFragment;
    }
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_activity,container,false);
        tv = (TextView) view.findViewById(R.id.fragment_tv);
        return view;
    }
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String str = getArguments().getString("TAG");
        tv.setText(str);
    }
}
