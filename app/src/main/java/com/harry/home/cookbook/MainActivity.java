package com.harry.home.cookbook;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.harry.home.cookbook.activity.SecondActivity;
import com.harry.home.cookbook.adapter.MyFragmentAdapter;

public class MainActivity extends AppCompatActivity {
    private ViewPager vp;
    private TabLayout tl;
    private String[] str = new String[]{"傻逼","白痴","豆逼","脑残","蠢货","弱智","智障"};
    private int[] mIcons = {R.mipmap.app_menu_fire_fault, R.mipmap.app_menu_fire_maintenance, R.mipmap.app_menu_fire_monitor,
            R.mipmap.app_menu_fire_waring, R.mipmap.app_menu_fire_fault, R.mipmap.app_menu_fire_maintenance, R.mipmap.app_menu_fire_monitor};
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vp = (ViewPager) findViewById(R.id.main_vp);
        tl = (TabLayout) findViewById(R.id.main_tl);
        vp.setAdapter(new MyFragmentAdapter(getSupportFragmentManager()));
        tl.setupWithViewPager(vp);
        int tabCount = tl.getTabCount();
        for (int i = 0; i < tabCount; i++) {
            TabLayout.Tab tab = tl.getTabAt(i);
            tab.setText(str[i]).setIcon(mIcons[i]);
        }
    }
    public void btn_interface(View view){
        startActivity(new Intent(this, SecondActivity.class));
    }
}
