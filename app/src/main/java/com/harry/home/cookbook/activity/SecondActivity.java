package com.harry.home.cookbook.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.harry.home.cookbook.R;

/**
 * Created by Administrator on 2016/7/16.
 */
public class SecondActivity extends AppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }
    public void btn_second(View view){
        switch(view.getId()){
            case R.id.btn_fab:
                startActivity(new Intent(this,FabActivity.class));
                break;
            case R.id.btn_col:
                startActivity(new Intent(this, ColActivity.class));
                break;
            case R.id.btn_app:
                startActivity(new Intent(this, AppActivity.class));
                break;
        }
    }
}
