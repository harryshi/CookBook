package com.harry.home.cookbook.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.harry.home.cookbook.fragment.TestFragment;

/**
 *
 * Created by Administrator on 2016/7/16.
 */
public class MyFragmentAdapter extends FragmentPagerAdapter{
    public MyFragmentAdapter(FragmentManager fm) {
        super(fm);
    }
    public Fragment getItem(int position) {
        return TestFragment.newInstance("这是第"+position+"个碎片");
    }
    public int getCount() {
        return 7;
    }
   /* public CharSequence getPageTitle(int position) {
        return str[position];
    }*/
}
