package com.harry.home.cookbook.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ImageView;

import com.harry.home.cookbook.R;

/**
 * Created by Administrator on 2016/7/16.
 */
public class FabActivity extends Activity{
    protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_fab);
        final ImageView iv = (ImageView) findViewById(R.id.iv);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(iv, "hello Snackbar", Snackbar.LENGTH_SHORT).show();
            }
        });
}

}
